import React, { Component } from 'react'
import ItemShoes from './ItemShoes'

export default class ListShoes extends Component {
  render() {
    return (
        <div className='container'>
          <div className='row'>
            {this.props.data.map((item, index) => {
              return(
                <div key={index} className='col-3'>
                  <ItemShoes                   
                  handleViewDetail={this.props.handleXemChiTiet}
                  detail={item}
                  handleAddToCart={this.props.handleAddToCart}
                  />
                </div>
              );
            })}
          </div>
        </div>
    );
  }
}
