import React, { Component, Fragment} from 'react'

export default class ItemShoes extends Component {
  render() {
    let {image, name, price, } = this.props.detail;

    let stybutton = {
        width: "110px",
        height: "40px",
        background: "black",
        color: "white",
        borderRadius: "4px",
        marginRight: "10px",
    };

    return (
      <Fragment>
      <div style={{marginBottom: 20}}>
        <div className="card" style={{width: '18rem', border: "1px solid black"}}>
            <div style={{borderBottom: "1px solid gray"}}>
            <img className="card-img-top" src={image} alt="Card image cap" />
            </div>
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <p className="card-text" style={{fontSize: 18, color: "red", fontWeight: 500}}>{price} $</p>
                <button
                onClick={() => {
                  this.props.handleAddToCart(this.props.detail);
                }} 
                 style={stybutton}>
                    add to carts
                 </button>

                <button 
                onClick={() => {
                  this.props.handleViewDetail(this.props.detail.id);
                }}
                 style={stybutton}>
                    xem chi tiết
                </button>
            </div>
        </div>
      </div>
      </Fragment>
    );
  }
}
