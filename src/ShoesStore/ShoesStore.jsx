import React, { Component } from 'react'
import { DataShoes } from '../Data_Shoes/Data_Shoes';
import DetailShoes from './DetailShoes';
import GioHangShoes from './GioHangShoes';
import ListShoes from './ListShoes';

export default class ShoesStore extends Component {
    state={
        shoesArray: DataShoes,
        detailShoes: DataShoes[0],
        gioHangShoes: [],
    };

    handleXemChiTiet = (idShoe) => {
      let index = this.state.shoesArray.findIndex((item) => {
        return item.id == idShoe;
      });
  
      index !== -1 &&
        this.setState({
          detailShoes: this.state.shoesArray[index],
        });
      // console.log(idShoe);
    };

    handleAddToCart = (shoes) => {
    let cloneGioHang = [...this.state.gioHangShoes];

    let index = this.state.gioHangShoes.findIndex((item) => {
      return item.id == shoes.id;
    });

    if (index == -1) {
      let spGioHang = { ...shoes, soLuong: 1 };
      cloneGioHang.push(spGioHang);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({
      gioHangShoes: cloneGioHang,
    });
    };

    handleTru = (shoes) => {
    let clonegiohang = [...this.state.gioHangShoes];
    let index = this.state.gioHangShoes.findIndex((item) => {
      return item.id == shoes;
    });
    if(this.state.gioHangShoes[index].soLuong == 1){
      console.log("không thể giảm");
    }else{
      clonegiohang[index].soLuong--;
    }
    this.setState({
      gioHangShoes: clonegiohang,
    });
    };

    handleCong = (shoes) => {
      let clonegiohang = [...this.state.gioHangShoes];
      let index = this.state.gioHangShoes.findIndex((item) => {
        return item.id == shoes;
      });
      clonegiohang[index].soLuong++;
      this.setState({
        gioHangShoes: clonegiohang,
      });
    };

    handleRemove = (shoes) => {
      let clonegiohang = [...this.state.gioHangShoes];
      let index = this.state.gioHangShoes.findIndex((item) => {
        return item.id == shoes;
      });
      clonegiohang.splice(index, 1);
      this.setState({
        gioHangShoes: clonegiohang,
      });
    };


  render() {
    // console.log("gio hang", this.state.gioHangShoes);
    return (
      <div>
        <GioHangShoes 
        giohang={this.state.gioHangShoes}
        handleTru={this.handleTru}
        handleCong={this.handleCong}
        handleRemove={this.handleRemove}
        />
        <ListShoes 
        data={this.state.shoesArray} 
        handleXemChiTiet={this.handleXemChiTiet}
        handleAddToCart={this.handleAddToCart}
        />
        <DetailShoes 
        detailShoes={this.state.detailShoes}
        />
      </div>
    )
  }
}

