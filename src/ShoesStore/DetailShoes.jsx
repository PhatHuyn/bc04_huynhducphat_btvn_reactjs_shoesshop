import React, { Component } from 'react'

export default class DetailShoes extends Component {
  render() {
    let {name, price, image, description, quantity} = this.props.detailShoes;
    let styTable = {
      witdh: "200px",
      align: "center", 
      border: "1px solid black",
      padding: "10px", 
    };

    return (
      <div className='container'>        
        <table style={styTable}>
          <tr style={styTable}>
            <th style={styTable}>Ảnh sản phẩm</th>
            <th colSpan={2}>thông tin sản phẩm</th>
          </tr>
          <tr>
            <td style={styTable} rowSpan={4}>
              <img src={image} alt=""/>
            </td>
            <td style={styTable}>name</td>
            <td style={styTable}>{name}</td>
          </tr>
          <tr>
            <td style={styTable}>giá</td>
            <td style={styTable}>{price}</td>
          </tr>
          <tr>
            <td style={styTable}>mô tả</td>
            <td>{description}</td>
          </tr>
          <tr>
            <td style={styTable}>số lượng</td>
            <td style={styTable}>{quantity}</td>
          </tr>
        </table>      
      </div>
    )
  }
}

